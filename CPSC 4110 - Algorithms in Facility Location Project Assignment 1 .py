import networkx as nx
import osmnx as ox
from geopy.geocoders import Nominatim

G = ox.graph_from_place("City of Lethbridge") # create a network constrained to the shape of City of Lethbridge
ox.plot_graph(G); # plot the graph on screen

in_degree_centrality_nodes = nx.in_degree_centrality(G) # networkx function to obtain dictionary of nodes with in-degree centrality value
max_in_degree_node = max(in_degree_centrality_nodes, key = lambda x: in_degree_centrality_nodes.get(x)) # Return a node possessing maximum in-degree centrality value
print("Node with the maximum in-degree ->", "ID:", max_in_degree_node, ", Value:", in_degree_centrality_nodes.get(max_in_degree_node))

G_projected = ox.project_graph(G) # project the network to an appropriate UTM (automatically determined)
nc = ['red' if node==max_in_degree_node else 'green' for node in G_projected.nodes()] # change the color of the node with max in-degree centrality
ns = [50 if node==max_in_degree_node else 7 for node in G_projected.nodes()] # change the size of the node with max in-degree centrality
ox.plot_graph(G_projected, node_size=ns, node_color=nc, node_zorder=2) # plot the graph on screen

print("Latitude and longitude coordinates of the maximum in-degree vertex:", "49.6755633, -112.8413942") # These values were obtained from OSM

geolocator = Nominatim(user_agent="my-application")
location = geolocator.reverse("49.6755633, -112.8413942") # Find the nearest location to the latitude and longitude
print("Closest human readable address:", location.address)

#end
#test